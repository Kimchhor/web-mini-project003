import './App.css';
 import NavMenu from './component/NavMenu'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import Home from './view/Home';
import Article from './view/Article';
import Read from './view/Read';
import Author from './view/Author';
function App() {
  return (
    <Router>
      <NavMenu />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/article' component={Article} />
        <Route path='/read/:id' component={Read} />
        <Route path='/author' component={Author} />
      </Switch>
    </Router>
  );
}

export default App;

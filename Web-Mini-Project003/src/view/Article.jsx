import React from "react";
import { useState } from "react";
import { Container, Form, Button } from "react-bootstrap";
import { postArticle } from "../service/article_service";
const Article = () => {

const [title, setTitle] = useState("")
const [description, setDescription] = useState("")
const [imageUrl, setImage] = useState("")
const onSave=(e)=>{
  e.preventDefault()
  let article={
    title,description,image: imageUrl
  }
  postArticle(article)
  
}
  return (
    <Container>
      <h1 className="my-4">Add Article</h1>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control value={title} onChange={(e)=>setTitle(e.target.value)} type="text" placeholder="Title" />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Author</Form.Label>
          <Form.Control type="text" placeholder="Author" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Image Url</Form.Label>
          <Form.Control value={imageUrl} onChange={(e)=>setImage(e.target.value)} type="text" placeholder="Image" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
          <Form.Label>Description</Form.Label>
          <Form.Control value={description} onChange={(e)=>setDescription(e.target.value)} as="textarea" rows={3} placeholder="Description" />
        </Form.Group>
        <Button onClick={onSave} variant="primary" type="submit">
          Post
        </Button>
      </Form>
    </Container>
  );
};

export default Article;

import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'

import {Container} from 'react-bootstrap'
import { onFetchArticleById } from '../redux/action/articleAction'
const Read = () => {
    const {id} = useParams()
    const dispatch =useDispatch(0)
    const article=useSelector(state => state.articleReducer.article)
    useEffect(() => {
        dispatch(onFetchArticleById(id))
        
    }, [])
    return (
        <Container>
            {
                article?
                <>
                <h3>{article.title}</h3>
                <img src={article.image} />
                <div></div>
                <h3>{article.description}</h3>
                </>
                
                : <h3>Data not found</h3>
            }
        </Container>
    )
}

export default Read

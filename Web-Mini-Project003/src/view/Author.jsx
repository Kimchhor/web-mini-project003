import React from 'react'
import {Container,Form,Button,Table} from 'react-bootstrap'
const Author = () => {
    return (
        <Container>
            <h1>Author</h1>
            <Form className="mb-3" >
  <Form.Group className="mb-3" >
    <Form.Label>Name</Form.Label>
    <Form.Control type="text" placeholder="Name" />
    
  </Form.Group>

  <Form.Group className="mb-3" >
    <Form.Label>Email</Form.Label>
    <Form.Control type="email" placeholder="Email" />
  </Form.Group>
  
  <Button variant="primary" type="submit"  >
    Save
  </Button>
</Form >

<Table striped bordered hover >
  <thead >
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Email</th>
      <th>Image</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>
        <Button variant="warning">Edit</Button>{' '}
        <Button variant="danger">Delete</Button>
      </td>
    </tr>
    
  </tbody>
</Table>
        </Container>
    )
}

export default Author

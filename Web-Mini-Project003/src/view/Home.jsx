import React from 'react'
import { useEffect } from 'react'
import {Row,Col, Container} from 'react-bootstrap'
import { useSelector,useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { onFetchArticle } from '../redux/action/articleAction'
import ArticleCard from '../component/ArticleCard'
const Home = () => {

    const articles= useSelector(state => state.articleReducer.articles)
    const dispatch=useDispatch()

    const onFetch=bindActionCreators(onFetchArticle,dispatch)
    useEffect(() => {
        onFetch().then(console.log)
    }, [])
    return (
        <Container>
            
            <Row>
                <h1 className="my-4">Category</h1>

                {
                    articles.map(article => 
                        <Col key={article._id} md={3}>
                    <ArticleCard article={article} />
                </Col>
                )
                }
            </Row>
        </Container>
    )
}

export default Home


import api from "../utils/api"

export const fetchArticle = async () =>{
    let response = await api.get('articles')
    return response.data
}
export const deleteArticle = async (id) =>{
    let response = await api.delete(`articles/${id}`)
    return response.data.message
}
export const postArticle = async (article) =>{
    let response = await api.post(`articles`,article)
    return response.data.message
}
export const fetchArticleById = async (id) =>{
    let response = await api.get(`articles/${id}`)
    return response.data
}
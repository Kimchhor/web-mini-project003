import {combineReducers} from 'redux'
import articleReducer from './articleReducer'
const reducers=combineReducers({
    articleReducer,
});

export default reducers;
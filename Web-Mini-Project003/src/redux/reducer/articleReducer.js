import {articleType } from '../action/actionType'
const initialState = {
    articles:[],
    article: null
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case articleType.FETCH_ARTICLE:
        return { ...state, articles: [...payload] }
    case articleType.DELETE_ARTICLE:
        return {...state, articles: state.articles.filter(article => article._id !== payload)}
    case articleType.FETCH_ARTICLE_BY_ID:
        return{...state,article:payload}
    default:
        return state
    }
}

import {articleType as type} from './actionType'

import Swal from 'sweetalert2'
import { deleteArticle, fetchArticle,fetchArticleById } from '../../service/article_service'

export const onFetchArticle = () => async dispatch =>{
    let response = await fetchArticle()

    dispatch({
        type: type.FETCH_ARTICLE,
        payload: response.data
    })
    return Promise.resolve(response.message)
}

export const onDeleteArticle = (id) => {

    return async(dispatch) => {
        Swal.fire({
            title: 'Are you sure to delete this?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Yes, delete it!" 
          }).then(async (result) => {
            if (result.isConfirmed) {
                await deleteArticle(id);
                dispatch({
                    type: type.DELETE_ARTICLE,
                    payload: id,
                });
                
            
            }
          })
    } 

}

export const onFetchArticleById=(id)=> async dispatch =>{
    let response = await fetchArticleById(id)
    dispatch({
        type:type.FETCH_ARTICLE_BY_ID,
        payload: response.data
    })
    return Promise.resolve(response.message)
}




export const articleType={
    FETCH_ARTICLE:"FETCH_ARTICLE",
    DELETE_ARTICLE:"DELETE_ARTICLE",
    FETCH_ARTICLE_BY_ID:"FETCH_ARTICLE_BY_ID"
}
import React from 'react'
import { Card,Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { onDeleteArticle } from '../redux/action/articleAction'
function ArticleCard({article}) {

    const dispatch = useDispatch()
    const onDelete=bindActionCreators(onDeleteArticle,dispatch)
    const history=useHistory()
    return (
        <div>
            <Card style={{ width: '18rem' }}>
  <Card.Img variant="top" style={{objectFit:"cover", height :"150px" }} src={article.image} />
  <Card.Body>
    <Card.Title>{article.title}</Card.Title>
    <Card.Text>{article.description}</Card.Text>
    <Button variant="info" onClick={()=>history.push('/read/'+ article._id)}>Read</Button>{' '}
    <Button variant="warning">Edit</Button>{' '}
    <Button variant="danger" onClick={()=> onDelete(article._id)}>Delete</Button>
  </Card.Body>
</Card>
        </div>
    )
}

export default ArticleCard

import React from 'react'
import {Nav,Navbar,Container,NavDropdown} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
 const NavMenu = () => {
    return (
        <Navbar bg="primary" variant="dark">
    <Container>
    <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
    <Nav className="me-auto">
      <Nav.Link as={Link} to="/">Home</Nav.Link>
      <Nav.Link as={NavLink} to="/article">Article</Nav.Link>
      <Nav.Link as={NavLink} to="/author" >Author</Nav.Link>
      <Nav.Link href="#pricing">Category</Nav.Link>
      <NavDropdown title="Language" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">English</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Khmer</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    
    </Container>
  </Navbar>
    )
};
export default NavMenu;

